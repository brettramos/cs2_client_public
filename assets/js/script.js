console.log("hello from scriptJS")
let token = localStorage.getItem('token');
let isAdmin = localStorage.getItem('isAdmin');
console.log('isAdmin: '+ isAdmin);

// if user is NOT logged in
if (!token || token === null){
	
   	console.log("NOT logged in FROM scriptJS");
   	//Profile link
   	const oldProfileBtn = document.querySelector("#navBarProfileSpan");
	//create <span> element to place Register link in navbar
	const navBarRegisterBtn = document.createElement('span');
	//create link element script
	navBarRegisterBtn.innerHTML = '<li class="nav-item active"><a href="./pages/register.html" class="nav-link" id="navBarRegister">Register</a></li>';	
	//replace Profile link with Register link
	try{
		oldProfileBtn.parentNode.replaceChild(navBarRegisterBtn, oldProfileBtn);
	} catch (err){
		console.log(err);
	}	

// if user is logged in
} else {

	//replace Register button with Profile button if registered user is logged in
	console.log("from scriptJS logged In")

	const oldLoginBtn = document.querySelector("#navBarLoginSpan");	
	const navBarLogOutBtn = document.createElement('span');
	navBarLogOutBtn.innerHTML = '<li class="nav-item"><a href="./pages/logout.html" class="nav-link" id="navBarLogOut">Log Out</a></li>';
	oldLoginBtn.parentNode.replaceChild(navBarLogOutBtn, oldLoginBtn);

	// user isAdmin
	if(isAdmin==='true'){
		document.querySelector('#navBarProfileSpan').style.visibility="hidden";
		console.log('HIDING Profile')

	// user is not Admin
	} else {
		document.querySelector('#navBarProfileSpan').style.visibility="visible";
		console.log('SHOWING Profile')

	}	
}

// LOGOUT
// console.log("hello from logoutJS");
try {
	let logOut = document.querySelector("#navBarLogOut")

    logOut.addEventListener("click", () => {
    //clear TOKEN from localStorage
    localStorage.clear();
    console.log("Logout successful");
    //go to logout page
    window.location.replace("./pages/logout.html");
    })
} catch (err) {
	console.log(err);
}