console.log("Hello from courseJS")

let courseTitle = document.querySelector('#courseTitle');
let courseDescription = document.querySelector('#courseDescription');
let coursePrice = document.querySelector('#coursePrice');
let courseStatus = document.querySelector('#courseStatus');
let adminDiv = document.querySelector('#adminDiv');
let enrolDiv = document.querySelector('#enrolDiv');
let editBtn = document.querySelector('#editBtn');
let deleteBtn = document.querySelector('#deleteBtn');

//TODO replace with dynamic courseID
// let courseId =  "5f8f9b64d2fb18159c9245db";
// localStorage.setItem('courseId', courseId);
console.log('from local: '+ localStorage.getItem('courseId'))


// let route = "http://localhost:3000/api/courses/" + localStorage.getItem('courseId');
let route = "https://morning-mountain-13833.herokuapp.com/api/courses/" + localStorage.getItem('courseId');


fetch(route, {
    method: 'GET',
    headers: {
        'Content-Type': 'application/json'
    }
})
.then(res => {
    return res.json()
})
.then(data => {
    
    console.log(data);
   				courseTitle.innerHTML = data.name;
                courseDescription.innerHTML = data.description;
                coursePrice.innerHTML = data.price;
                
                if(localStorage.getItem('isAdmin') === 'true'){

	                if(data.isActive === true){
	                	adminDiv.style.visibility = "initial";
	                	enrolDiv.style.visibility = "hidden"
	                	courseStatus.innerHTML = 'active'
	                } else {
	                	courseStatus.innerHTML = 'inactive'
	                }
	                console.log(data._id)
            	}else{
            		adminDiv.style.visibility = "hidden"
            		enrolDiv.style.visibility = "initial"
            	}
                                
})

editCourseForm.addEventListener("submit", () => {
	window.location.replace("./editCourse.html");
})
    
console.log("hello from courseJS END")