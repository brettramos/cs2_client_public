console.log("hello from loginjs");
let loginForm = document.querySelector("#loginForm")

loginForm.addEventListener("submit", (e) => {
    e.preventDefault()

  let email = document.querySelector("#loginEmail").value;
  let password = document.querySelector("#loginPassword").value; 

  //create a validation that will allow us to determine if the input fields are not empty
    if(email == "" || password == "") {
        alert("Please input your email and password")
    } else {
        // fetch('http://localhost:3000/api/users/login', {
        fetch('https://morning-mountain-13833.herokuapp.com/api/users/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => {
            return res.json()
        })
        .then(data => {
            //to check if the data has been caught, lets display it first in the console
            // console.log(data)
            //upon succesful authentication it will return the JWT
            //lets create a control structure to determine if the JWT has been generated together with the user credentials in an object format
            if(data.access){
                //we are going to store now the JWT inside our localStorage
                localStorage.setItem('token', data.access)
                // fetch('http://localhost:3000/api/users/details', {
                fetch('https://morning-mountain-13833.herokuapp.com/api/users/details', {
                    headers: {
                        Authorization: `Bearer ${data.access}`
                    }
                }).then(res => {
                    return res.json()
               
                }).then(data => {
                    localStorage.setItem('id', data._id),
                    localStorage.setItem('isAdmin', data.isAdmin)
                    //once the id is authenticated succesfully using the access token, then it shouhd redirect the user to the user's profile page if user is NOT admin. If user is admin, direct to home page
                    if(data.isAdmin){
                        window.location.replace("../index.html")

                    } else {
                        window.location.replace("./profile.html") 
                    }                    

                })
                
            } else {
                //create an else branch that will run if an access key is not found
                alert("Something went wrong. Check your credentials!")
            }
        })
    }
})
