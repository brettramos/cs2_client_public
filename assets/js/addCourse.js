console.log('Hello from registerJS');
let createCourseBtn = document.querySelector("#createCourseBtn")

let createCourseForm = document.querySelector("#createCourseForm")

createCourseForm.addEventListener("submit", (e) => {
    e.preventDefault()
    
    let name = document.querySelector("#courseName").value
    let description = document.querySelector("#courseDescription").value
    let price = document.querySelector("#coursePrice").value

    console.log(name);
    console.log(description);
    console.log(price);

    //lets create a validation to enable the add course button when all fields are populated
    if((name !== '' && description !== '' && price !== '')) {

        if (!token || token === null){

        } else {
            fetch('https://morning-mountain-13833.herokuapp.com/api/courses', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify({
                            name: name,
                            description: description,
                            price : price
                })     
            })
            .then(res => {
                return res.json()
            })
            .then(data => {
                console.log(data)
                    //lets give a proper response if course aaddition will become successful
                    if(data === true){
                        console.log("successful COURSE registration");
                        alert("New Course Added Successfully");
                     
                        window.location.replace("./courses.html")     
                    } else {
                        
                        alert("Something Went Wrong in the Course Registration!")
                        console.log("something went wrong1")
                    }                
            })
        }
    } else {
        alert("Something went wrong, check course details!");
        console.log("something went wrong2")  
    }
})