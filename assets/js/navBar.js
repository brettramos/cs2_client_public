console.log("Hello from navBarJS");
let isAdmin = localStorage.getItem('isAdmin');
let token = localStorage.getItem('token');
console.log('isAdmin: '+ isAdmin);


// if user is NOT logged in
if (!token || token === null){   
   	console.log("NOT logged in FROM navBarJS");
   	//Profile link
   	const oldProfileBtn = document.querySelector("#navBarProfileSpan");
	//create <span> element to place Register link in navbar
	const navBarRegisterBtn = document.createElement('span');
	//create link element script
	navBarRegisterBtn.innerHTML = '<li class="nav-item active"><a href="./register.html" class="nav-link" id="navBarRegister">Register</a></li>';
	
	//replace Profile link with Register link
	try{
		oldProfileBtn.parentNode.replaceChild(navBarRegisterBtn, oldProfileBtn);
	} catch (err){
		console.log(err);
	}	
//if user is logged in
} else {

	console.log("logged in FROM navBarJS")

	// user isAdmin
	if(isAdmin==='true'){
		try{
			document.querySelector('#navBarProfileSpan').style.visibility="hidden";
			console.log('HIDING Profile')
		} catch (err){
			console.log(err);
		}
		
	// user is not Admin
	} else {
		try{
			document.querySelector('#navBarProfileSpan').style.visibility="visible";
			console.log('SHOWING Profile');
		} catch (err){
			console.log(err);
		}
	}	

	//Login link
	const loginBtn = document.querySelector("#navBarLoginSpan");
	//create <span> element to place Log Out link in navbar
	const navBarLogOutBtn = document.createElement('span');
	//create link element script
	navBarLogOutBtn.innerHTML = '<li class="nav-item"><a  class="nav-link" id="navBarLogOut">Log Out</a></li>';
	//replace Login link with Logout link
	loginBtn.parentNode.replaceChild(navBarLogOutBtn, loginBtn);	

	

	
	
}

// LOGOUT
// console.log("hello from logoutJS");
try {
	let logOut = document.querySelector("#navBarLogOut")

    logOut.addEventListener("click", () => {

    	if(confirm("Are you sure you want to log out?")){    	
    		localStorage.clear();
    		window.location.replace('../index.html');
    	} else {
    		console.log("NOT logout")
    	}
    
    })
} catch (err) {
	console.log(err);
}




//OLD LOGOUT CODE
// try {
// 	let logOut = document.querySelector("#navBarLogOut")

//     logOut.addEventListener("click", () => {

//     localStorage.clear();
//     window.location.replace('../index.html');
//     })
// } catch (err) {
// 	console.log(err);
// }