console.log('Hello from registerJS');
let registerForm = document.querySelector("#registerUser")

registerForm.addEventListener("submit", (e) => {
    e.preventDefault()
    
    let firstName = document.querySelector("#firstName").value
    let lastName = document.querySelector("#lastName").value 
    let mobileNo = document.querySelector("#mobileNo").value
    let email = document.querySelector("#userEmail").value
    let password1 = document.querySelector("#password1").value
    let password2 = document.querySelector("#password2").value

    let password = document.getElementById("password")
    let confirm_password = document.getElementById("confirm_password");

    if((password1 !== '' && password2 !== '') && (password2 === password1) && (mobileNo.length === 11)) {

        //if all the requirements are met, then now lets check for duplicate emails in the database first. 
        fetch('https://morning-mountain-13833.herokuapp.com/api/users/email-exists', {
            method: 'POST', 
            headers: {
                'Content-Type': 'application/json'
            }, 
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {
            //if no email duplicates are found 
            if(data === false){
                fetch('https://morning-mountain-13833.herokuapp.com/api/users', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        email : email,
                        password: password1,
                        mobileNo: mobileNo
                    })
                })
                .then(res => {
                    return res.json()
                })
                .then(data => {
                    console.log(data)
                    //lets give a proper response if registration will become successful
                    if(data === true){
                        console.log("successful registration");
                        alert("New Account Registered successfully");
                        //redirect to login 
                        window.location.replace("./login.html")                        
                        // window.location.replace("../index.html");
                    } else {
                        
                        alert("Something Went Wrong in the Registration!")
                        console.log("something went wrong1")
                    }
                })
            }
        })
    } else {
      alert("Something went wrong, check credentials!");
      console.log("something went wrong2")  
    }
})