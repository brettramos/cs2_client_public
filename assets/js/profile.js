console.log("Hello from profileJS");

let profFirstName = document.querySelector('#profFirstName');
let profLastName = document.querySelector('#profLastName');
let profEmail = document.querySelector('#profEmail');
let profMobileNo = document.querySelector('#profMobileNo');

if (!token || token === null){

	// window.location.href="./login.html";

    } else {

        if(localStorage.getItem('isAdmin') === 'true'){

            //display respective information to HTML
            profFirstName.innerHTML = 'ADMIN';
            profLastName.innerHTML = 'ADMIN';
            profEmail.innerHTML = 'ADMIN';
            profMobileNo.innerHTML = 'ADMIN'; 

        } else {

            // fetch('http://localhost:3000/api/users/details', {
            fetch('https://morning-mountain-13833.herokuapp.com/api/users/details', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                },     
            })
            .then(res => {
                return res.json()
            })
            .then(data => {                
                let enrollmentData = data.enrollments.map(classData => {
                    console.log(classData);
                    return(
    `
                    <tr>
                       <td>${classData.courseId}</td> 
                       <td>${classData.enrolledOn}</td> 
                       <td>${classData.status}</td> 
                    </tr> 
                    `
                     )

                }).join('')
                    profileContainer.innerHTML = 
                `<div class="com-md-12">
                    <section class="jumbotron my-0 bg-transparent">
                        
                        <h3 class="text-center">Class History</h3>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="text-white"> CourseID </th>
                                    <th class="text-white"> Enrolled On </th>
                                    <th class="text-white"> Status </th>
                                </tr>
                                <tbody>
                                    ${enrollmentData}
                                </tbody>
                            </thead>
                        </table>
                    </section>
                </div>
            `   
                //display respective information to HTML
                profFirstName.innerHTML = data.firstName;
                profLastName.innerHTML = data.lastName;
                profEmail.innerHTML = data.email;
                profMobileNo.innerHTML = data.mobileNo;       
            })
        }
    }
