console.log('hello from NEWcoursesJS')
let courseContainer = document.querySelector('#courseContainer');
let addCourseForm = document.querySelector('#addCourseForm');

if(localStorage.getItem('isAdmin') === 'true'){
    addCourseForm.style.visibility="initial";

    // fetch('http://localhost:3000/api/courses', {
    fetch('https://morning-mountain-13833.herokuapp.com/api/courses', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(res => {
        return res.json()
    })
    .then(data => {
        console.log(data);
        let courseData = data.map(courseData => {
                    // console.log(courseData);
                    return(
    `

    <div class="row my-2 border courseCard">
        <div class="col-lg-6">
            <img class="card-img-top" src="https://via.placeholder.com/200x100" alt="Course Image">
        </div>
        <div class="col-lg-6">
            <div class="card-body">
              <h5 class="card-title">${courseData.name}</h5>
              <p class="card-text cardCourseDesc">${courseData.description}</p>
              <p class="card-text">${courseData.price}</p>
              <p class="card-text ${isCourseActive(courseData.isActive)}</p>
            </div>              
            <div class="card-body">
              <a class="card-link" onclick="goToCourse('${courseData._id}')">View</a>
            </div>
        </div>        
    </div>
           
    `
                     )
                }).join('')
                    courseContainer.innerHTML = 
                `<div class="col-md-12">
                    <section class="jumbotron my-0 bg-transparent">
                     
                                    ${courseData}
                             
                    </section>
                </div>
            `                
    })
} else {
    addCourseForm.style.visibility="hidden";
    // fetch('http://localhost:3000/api/courses/active', {
    fetch('https://morning-mountain-13833.herokuapp.com/api/courses/active', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(res => {
        return res.json()
    })
    .then(data => {
        console.log(data);
        let courseData = data.map(courseData => {
                    // console.log(courseData);
                    return(
    `
    <div class="row my-2 border courseCard">
        <div class="col-md-6">
            <img class="card-img-top" src="https://via.placeholder.com/200x100" alt="Course Image">
        </div>
        <div class="col-md-6">
            <div class="card-body">
              <h5 class="card-title">${courseData.name}</h5>
              <p class="card-text">${courseData.description}</p>
              <p class="card-text">${courseData.price}</p>
            </div>              
            <div class="card-body">
              <a class="card-link" onclick="goToCourse('${courseData._id}')">View</a>
              <a href="#" class="card-link">Enrol</a>
            </div>
        </div>        
    </div>
           
    `
                     )
                }).join('')
                    courseContainer.innerHTML = 
                `<div class="col-md-12">
                    <section class="jumbotron my-0 bg-transparent">
                     
                                    ${courseData}
                             
                    </section>
                </div>
            `                
    })

}    
function isCourseActive(isActive){

    if(isActive){
        return ' isActive"> Active';
    } else{
        return ' isInactive"> Inactive';
    }
}
function goToCourse(courseId){
    console.log(localStorage.getItem('courseId'))
    localStorage.setItem('courseId', courseId);
    console.log('courseId set to LOCAL storage')
    window.location.replace("./course.html") 
}