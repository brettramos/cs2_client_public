console.log("Hello from editCourseJS")

let currTitle = document.querySelector('#currTitle');
let currPrice = document.querySelector('#currPrice');
let currDesc = document.querySelector('#currDesc');

console.log('from local: '+ localStorage.getItem('courseId'))

let route = "https://morning-mountain-13833.herokuapp.com/api/courses/" + localStorage.getItem('courseId');

fetch(route, {
    method: 'GET',
    headers: {
        'Content-Type': 'application/json'
    }
})
.then(res => {
    return res.json()
})
.then(data => {
    
    console.log(data);
   				currTitle.innerHTML = data.name;
                currDesc.innerHTML = data.description;
                currPrice.innerHTML = data.price;
               
                                
})

let editCourseForm = document.querySelector("#editCourseForm")

editCourseForm.addEventListener("submit", (e) => {
    e.preventDefault()
    
    let name = document.querySelector("#courseNewName").value
    let description = document.querySelector("#courseNewDescription").value
    let price = document.querySelector("#courseNewPrice").value

    console.log(name);
    console.log(description);
    console.log(price);

    //lets create a validation to enable the edit course button when all fields are populated
    if((name !== '' && description !== '' && price !== '')) {

        if (!token || token === null){

        } else {
            fetch('https://morning-mountain-13833.herokuapp.com/api/courses', {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify({
                            name: name,
                            description: description,
                            price : price
                })     
            })
            .then(res => {
                return res.json()
            })
            .then(data => {
                console.log(data)
                    //lets give a proper response if course aaddition will become successful
                    if(data === true){
                        console.log("successful COURSE edit");
                        alert("Course edited Successfully");
                     
                        window.location.replace("./courses.html")     
                    } else {
                        
                        alert("Something Went Wrong in the Course Modification!")
                        console.log("something went wrong1")
                    }                
            })
        }
    } else {
        alert("Something went wrong, check course details!");
        console.log("something went wrong2")  
    }
})


    
console.log("hello from editCourseJS END")